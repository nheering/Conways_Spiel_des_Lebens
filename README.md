# My version of Conways Game of life

I tried to implement Conways Game of life in Python. \
The Idea was to create a version with the no fancy external stuff. So with as little as external libraries as needed.

I did it by just using tkinter and no classes.

So in the case some of you don't know the Game of life it's a cellular automaton. So a simulation of cells that become alive, reproduce or die on four simple rules that are run in generations on a random generation zero on a defined grid in my case. The optimum would be an infinit area.

So the rules if a cell stays alive, dies or is born in a new generation are the following.

1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
2. Any live cell with two or three live neighbours lives on to the next generation.
3. Any live cell with more than three live neighbours dies, as if by overpopulation.
4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

For a deeper dive into the theory and the different kinds of Conways Game of life I suggest one
of the very great articles e.g. on wikipedia.