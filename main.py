# My first Version of  Conways Game of life
# Rules:
# 1. Any live cell with fewer than two live neighbours dies, as if by underpopulation.
# 2. Any live cell with two or three live neighbours lives on to the next generation.
# 3. Any live cell with more than three live neighbours dies, as if by overpopulation.
# 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

from tkinter import *
import random

bg_color = ["black", "blue"]
cell_width = 2
cell_height = 1
cell_border_width = 1
STICKY = W + N + E + S
cell_status = {}


def clean_gamearea():
    for widget in lblframe_gamearea.winfo_children():
        widget.destroy()


def draw_gamearea(x_achse, y_achse):
    clean_gamearea()
    for x in range(x_achse):
        for y in range(y_achse):
            Label(lblframe_gamearea,
                  text="",
                  bg=bg_color[cell_status[x][y]],
                  width=cell_width,
                  height=cell_height,
                  borderwidth=cell_border_width, relief="ridge"
                  ).grid(
                row=y,
                column=x)


def first_generation(x_achse, y_achse, startcells):
    # first generate dead cells for all positions
    for x in range(x_achse):
        cell_status[x] = {}
        for y in range(y_achse):
            cell_status[x][y] = 0

    for cell in range(startcells):
        cell_status[random.randint(0, x_achse - 1)][random.randint(0, y_achse - 1)] = 1

    return cell_status


def next_generation(x_achse, y_achse):
    neighbours = [(-1, -1), (0, -1), (1, -1),
                  (-1, 0), (1, 0),
                  (-1, 1), (0, 1), (1, 1)]
    new_cell_status = cell_status  # we need a new dictionary because we need the old state kept in mind for checks

    # run over every cell and check the neighbours and maybe set the new state
    for x in range(x_achse):
        for y in range(y_achse):
            alive = 0
            for nb in neighbours:
                nb_x = x - nb[0]
                nb_y = y - nb[1]

                # check if neighbour is out of gamearea
                if nb_x < 0:
                    nb_x = x_achse - 1
                elif nb_x >= x_achse:
                    nb_x = 0

                if nb_y < 0:
                    nb_y = y_achse - 1
                elif nb_y >= y_achse:
                    nb_y = 0

                if cell_status[nb_x][nb_y] == 1:
                    alive += 1

            # Now check if the cell stays alive or dies
            current_cell_status = cell_status[x][y]
            new_cell_status[x][y] = current_cell_status
            if current_cell_status == 0 and alive == 3:  # dead cell with 3 neighbours is born
                new_cell_status[x][y] = 1

            if current_cell_status == 1 and (
                    alive < 2 or alive > 3):  # lifing cell with less than 2 or more than 3 neighbours dies
                new_cell_status[x][y] = 0

    return new_cell_status


def run_game():
    x_achse = int(etr_xAchse.get())
    y_achse = int(etr_yAchse.get())
    generations = int(etr_generations.get())
    startcells = int(etr_startcells.get())

    first_generation(x_achse, y_achse, startcells)
    draw_gamearea(x_achse, y_achse)
    lbl_status['text'] = "Generation: 0"
    wnd_main.update()

    for gen in range(generations):
        next_generation(x_achse, y_achse)
        draw_gamearea(x_achse, y_achse)
        lbl_status['text'] = "Generation: " + str(gen + 1)
        wnd_main.update()


if __name__ == '__main__':
    wnd_main = Tk()
    wnd_main.title("Conways Game of life")

    wnd_main.configure(bg=bg_color[0])  # set background of window to the same color as the dead cells

    # Create settings area
    lblframe_options = LabelFrame(wnd_main, text="Options")
    lbl_xAchse = Label(lblframe_options, text="x-Axis:", anchor="w")
    etr_xAchse = Entry(lblframe_options, width=5)
    lbl_yAchse = Label(lblframe_options, text="y-Axis:", anchor="w")
    etr_yAchse = Entry(lblframe_options, width=5)
    lbl_generations = Label(lblframe_options, text="Generations:", anchor="w")
    etr_generations = Entry(lblframe_options, width=5)
    lbl_startcells = Label(lblframe_options, text="alive Cells at beginning", anchor="w")
    etr_startcells = Entry(lblframe_options, width=5)
    btn_start = Button(lblframe_options, text="Start", command=run_game)
    lbl_status = Label(lblframe_options, text="", anchor="w")

    lbl_xAchse.grid(row=0, column=0, padx=5, pady=5, sticky=STICKY)
    etr_xAchse.grid(row=0, column=1, padx=5, pady=5, sticky=STICKY)
    lbl_yAchse.grid(row=0, column=2, padx=5, pady=5, sticky=STICKY)
    etr_yAchse.grid(row=0, column=3, padx=5, pady=5, sticky=STICKY)
    lbl_generations.grid(row=0, column=4, padx=5, pady=5, sticky=STICKY)
    etr_generations.grid(row=0, column=5, padx=5, pady=5, sticky=STICKY)
    lbl_startcells.grid(row=0, column=6, padx=5, pady=5, sticky=STICKY)
    etr_startcells.grid(row=0, column=7, padx=5, pady=5, sticky=STICKY)
    btn_start.grid(row=1, column=0, padx=5, pady=5, columnspan=4, sticky=STICKY)
    lbl_status.grid(row=1, column=4, padx=5, pady=5, columnspan=3, sticky=STICKY)

    #  set some basic values so that we can start right away
    etr_xAchse.insert(0, '50')
    etr_yAchse.insert(0, '30')
    etr_generations.insert(0, '15')
    etr_startcells.insert(0, '200')

    lblframe_gamearea = LabelFrame(wnd_main, bg=bg_color[0])  # Background of Gamearea is the same as a dead cell

    lblframe_options.pack(fill="both")
    lblframe_gamearea.pack(fill="both")

    mainloop()
